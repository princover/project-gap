import os
# import magic
import urllib.request
#from app import app
from flask import *
#from flask import Flask, flash, request, redirect, render_template, url_for
from werkzeug.utils import secure_filename
from cleandata import read_files
#local
read = read_files
app = Flask(__name__)
app.secret_key = "secret key"
ALLOWED_EXTENSIONS = set(['*'])
def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS
@app.route('/')
def upload_form():
    return render_template('upload.html')
@app.route('/upload', methods=['POST'])
def upload_file():
    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files:
           
            return redirect('/')
        file = request.files['file']
        if file.filename == '':
            flash('No file selected for uploading')
            return redirect('/')
        if file and allowed_file(file.filename):
            # filename = secure_filename(file.filename)
            # file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            f = request.files['file']
            path = 'C:\\Users\\LENOVO\\Desktop\\FakeData\\MyProject\\Endgame\\upload\\static\\uploads\\' + f.filename
            f.save(path)
            cd = read.fff(path)
            flash('File successfully uploaded')
            return redirect('/')
        else:
            flash('Allowed file type is xlsx')
            return redirect('/')
    return redirect('/')
if __name__ == "__main__":
    app.run(debug = True)