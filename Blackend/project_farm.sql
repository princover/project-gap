-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 13, 2020 at 04:41 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `project farm`
--

-- --------------------------------------------------------

--
-- Table structure for table `address`
--

CREATE TABLE `address` (
  `ID` int(10) NOT NULL,
  `HouseNo` varchar(10) NOT NULL,
  `Moo` varchar(2) NOT NULL,
  `Soi` varchar(20) DEFAULT NULL,
  `Road` varchar(50) NOT NULL,
  `Tambon` varchar(50) NOT NULL,
  `Amphure` varchar(30) NOT NULL,
  `Province` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `address house reg`
--

CREATE TABLE `address house reg` (
  `ID` int(10) NOT NULL,
  `HouseNo` varchar(10) NOT NULL,
  `Moo` varchar(2) NOT NULL,
  `Soi` varchar(20) DEFAULT NULL,
  `Road` varchar(50) NOT NULL,
  `Tambon` varchar(50) NOT NULL,
  `Amphure` varchar(30) NOT NULL,
  `Province` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `checklist`
--

CREATE TABLE `checklist` (
  `No` int(11) NOT NULL,
  `Watersource` varchar(30) NOT NULL,
  `Waterqualitytest` varchar(10) NOT NULL,
  `Amountofwater` varchar(10) NOT NULL,
  `Sourceofpollution` varchar(20) NOT NULL,
  `Distance` varchar(20) NOT NULL,
  `Watertransfer` varchar(5) NOT NULL,
  `Picture` varchar(10) NOT NULL,
  `traffic` varchar(15) NOT NULL,
  `road` varchar(40) NOT NULL,
  `Publicutility` varchar(20) NOT NULL,
  `electricity` int(11) NOT NULL,
  `waterused` varchar(50) NOT NULL,
  `otherutilities` varchar(100) NOT NULL,
  `Backuย` varchar(50) NOT NULL,
  `Soilbottomingconditions` varchar(40) NOT NULL,
  `Exterminationofaquaticpests` varchar(40) NOT NULL,
  `Disinfectionofponds` varchar(40) NOT NULL,
  `Otheraction` varchar(50) NOT NULL,
  `Waterfiltering` varchar(40) NOT NULL,
  `Disinfectionofwater` varchar(40) NOT NULL,
  `Waterqualityadjustment` varchar(40) NOT NULL,
  `OtherManagement` varchar(40) NOT NULL,
  `Numberofoffspring` varchar(20) NOT NULL,
  `Averagesize` int(4) NOT NULL,
  `waterfromfarm` varchar(10) NOT NULL,
  `ReportWasteWater` varchar(40) NOT NULL,
  `waterquality` varchar(40) NOT NULL,
  `Complaint` varchar(10) NOT NULL,
  `Cleanlinessofgrassaroundponds` varchar(40) NOT NULL,
  `Equipmentmaintenance` varchar(50) NOT NULL,
  `PreventionofContamination` varchar(10) NOT NULL,
  `Usefreshmanure` varchar(40) NOT NULL,
  `Implementationprocedures` varchar(50) NOT NULL,
  `Storagelocation` varchar(30) NOT NULL,
  `Containerused` varchar(30) NOT NULL,
  `Storagecondition` varchar(50) NOT NULL,
  `Moistureproof` varchar(10) NOT NULL,
  `Orderly` varchar(30) NOT NULL,
  `Management` varchar(30) NOT NULL,
  `eradicationsystem` varchar(10) NOT NULL,
  `PreparationofPonds` varchar(40) NOT NULL,
  `Cleaningequipment` varchar(40) NOT NULL,
  `aquatichealth` varchar(40) NOT NULL,
  `Howtocleanwater` varchar(40) NOT NULL,
  `Sendingaquaticanimals` varchar(40) NOT NULL,
  `Notification` varchar(30) NOT NULL,
  `Carcassmanagement` varchar(50) NOT NULL,
  `Wastewatermanagement` varchar(40) NOT NULL,
  `Splitsystem` varchar(30) NOT NULL,
  `Cleanlinesssystem` varchar(20) NOT NULL,
  `Bathroom` varchar(50) NOT NULL,
  ` Hygiene` varchar(30) NOT NULL,
  ` Contamination` varchar(30) NOT NULL,
  `Garbagecollectionsystem` varchar(40) NOT NULL,
  `Scrapingoffarmanimals` varchar(30) NOT NULL,
  `Contagion` varchar(30) NOT NULL,
  `Prevention` varchar(50) NOT NULL,
  `Harvesting` varchar(20) NOT NULL,
  `catchtype` varchar(10) NOT NULL,
  `howtocatch` varchar(10) NOT NULL,
  `catchplan` varchar(50) NOT NULL,
  `cleaningafter` varchar(10) NOT NULL,
  `sortingarea` varchar(10) NOT NULL,
  `Sortingdetails` varchar(50) NOT NULL,
  `MD` varchar(40) NOT NULL,
  `FMD` varchar(40) NOT NULL,
  `OtherMDFMD` varchar(40) NOT NULL,
  `Formofsale` varchar(40) NOT NULL,
  `Transportation` varchar(40) NOT NULL,
  `Methodandmaintaintemperature` varchar(50) NOT NULL,
  ` Aquaticmeat` varchar(30) NOT NULL,
  `Document` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `chemical`
--

CREATE TABLE `chemical` (
  `No` int(2) NOT NULL,
  `Name` varchar(30) NOT NULL,
  `ChemRegNo` varchar(20) NOT NULL,
  `Amount` varchar(3) NOT NULL,
  `ProductDate` date NOT NULL,
  `Exp` date NOT NULL,
  `Contamination` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `drug`
--

CREATE TABLE `drug` (
  `No` int(4) NOT NULL,
  `Name` varchar(30) NOT NULL,
  `Amount` varchar(10) NOT NULL,
  `DrugDate` date NOT NULL,
  `DrugExp` date NOT NULL,
  `DrugRegNo` varchar(20) NOT NULL,
  `LabeltoUse` varchar(10) NOT NULL,
  `TakeSpecified` varchar(10) NOT NULL,
  `LatestUse` date NOT NULL,
  `StopUse` date NOT NULL,
  `MedicationRec` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

CREATE TABLE `employee` (
  `ID` varchar(13) NOT NULL,
  `Fname_th` varchar(50) NOT NULL,
  `Lname_th` varchar(50) NOT NULL,
  `Fname_en` varchar(50) NOT NULL,
  `Lname_en` varchar(50) NOT NULL,
  `Date` date NOT NULL,
  `Tel` varchar(10) NOT NULL,
  `LineID` varchar(30) DEFAULT NULL,
  `Email` varchar(50) NOT NULL,
  `FarmID` int(10) NOT NULL,
  `addressReg` int(10) NOT NULL,
  `address` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `event`
--

CREATE TABLE `event` (
  `No` int(10) NOT NULL,
  `Name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `farm`
--

CREATE TABLE `farm` (
  `ID` int(10) NOT NULL,
  `Farmname` varchar(50) NOT NULL,
  `FarmNo` varchar(50) NOT NULL,
  `Moo` varchar(2) NOT NULL,
  `Tambon` varchar(50) NOT NULL,
  `Amphure` varchar(30) NOT NULL,
  `Province` varchar(20) NOT NULL,
  `Location` varchar(30) NOT NULL,
  `FarmPlan` varchar(10) NOT NULL,
  `registration` varchar(30) NOT NULL,
  ` Document` varchar(30) NOT NULL,
  `File` varchar(10) NOT NULL,
  `StartOperation` date NOT NULL,
  `Ownership` varchar(40) NOT NULL,
  `Area` int(3) NOT NULL,
  `Clarifier` varchar(40) NOT NULL,
  `StorageTank` varchar(40) NOT NULL,
  `SewageSystem` varchar(40) NOT NULL,
  `Pondbreed` varchar(40) NOT NULL,
  `Culture` varchar(20) NOT NULL,
  `CultArea` int(3) NOT NULL,
  `LastYearProduct` int(4) NOT NULL,
  `Pastณnvestment` varchar(40) NOT NULL,
  `FarmStatus` varchar(50) NOT NULL,
  `DataRec` varchar(50) NOT NULL,
  `ChecklistNo` int(11) NOT NULL,
  `Pond` int(11) NOT NULL,
  `Status` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `feed`
--

CREATE TABLE `feed` (
  `No` int(4) NOT NULL,
  `Name` varchar(40) NOT NULL,
  `FoodRegNo` varchar(20) NOT NULL,
  `TimeofFeed` int(2) NOT NULL,
  `DateandExpFood` varchar(10) NOT NULL,
  `Foodtest` varchar(15) NOT NULL,
  `Nutrition` varchar(10) NOT NULL,
  ` FoodOrder` int(10) NOT NULL,
  ` SuppName` varchar(50) NOT NULL,
  `SuppType` varchar(20) NOT NULL,
  `SuppRegNo` varchar(20) NOT NULL,
  `DateandExpSupp` varchar(10) NOT NULL,
  `LabeltoUse` varchar(10) DEFAULT NULL,
  `MaterialName` varchar(200) NOT NULL,
  `Contamination` varchar(20) NOT NULL,
  `Banned` varchar(10) NOT NULL,
  `Drug` int(4) NOT NULL,
  `Chemical` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `fishpond`
--

CREATE TABLE `fishpond` (
  `No` int(11) NOT NULL,
  `PondType` int(1) NOT NULL,
  `Size` int(3) NOT NULL,
  `Species` int(5) NOT NULL,
  `Age` int(3) NOT NULL,
  `FirstHarvest` date NOT NULL,
  `SecHarvest` date NOT NULL,
  `ThirdHarvest` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `fishtype`
--

CREATE TABLE `fishtype` (
  `No` int(5) NOT NULL,
  `FishName` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `pondtype`
--

CREATE TABLE `pondtype` (
  `No` int(1) NOT NULL,
  `Name` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pondtype`
--

INSERT INTO `pondtype` (`No`, `Name`) VALUES
(1, 'บ่อ'),
(2, 'กระชัง');

-- --------------------------------------------------------

--
-- Table structure for table `problem`
--

CREATE TABLE `problem` (
  `No` int(4) NOT NULL,
  `Problem` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `report`
--

CREATE TABLE `report` (
  `No` int(5) NOT NULL,
  `Event` int(4) NOT NULL,
  `Problem` int(4) NOT NULL,
  `PondProblem` int(11) NOT NULL,
  `Date` date NOT NULL,
  ` ResponsiblePerson` varchar(50) NOT NULL,
  `Treatment` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `status`
--

CREATE TABLE `status` (
  `No` int(2) NOT NULL,
  `Status` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `status`
--

INSERT INTO `status` (`No`, `Status`) VALUES
(1, 'ผ่าน'),
(2, 'รอการปรับปรุงแก้ไข'),
(3, 'ไม่ผ่านเกณฑ์');

-- --------------------------------------------------------

--
-- Table structure for table `treatment`
--

CREATE TABLE `treatment` (
  `No` int(4) NOT NULL,
  `Solve` varchar(200) NOT NULL,
  `EditingPeriod` int(4) NOT NULL,
  `Revival` varchar(200) NOT NULL,
  `RecoveryPeriod` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `address`
--
ALTER TABLE `address`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `address house reg`
--
ALTER TABLE `address house reg`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `checklist`
--
ALTER TABLE `checklist`
  ADD PRIMARY KEY (`No`);

--
-- Indexes for table `chemical`
--
ALTER TABLE `chemical`
  ADD PRIMARY KEY (`No`);

--
-- Indexes for table `drug`
--
ALTER TABLE `drug`
  ADD PRIMARY KEY (`No`);

--
-- Indexes for table `employee`
--
ALTER TABLE `employee`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `address` (`address`),
  ADD KEY `addressReg` (`addressReg`),
  ADD KEY `FarmID` (`FarmID`);

--
-- Indexes for table `event`
--
ALTER TABLE `event`
  ADD PRIMARY KEY (`No`);

--
-- Indexes for table `farm`
--
ALTER TABLE `farm`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ChecklistNo` (`ChecklistNo`),
  ADD KEY `บ่อ` (`Pond`),
  ADD KEY `Status` (`Status`);

--
-- Indexes for table `feed`
--
ALTER TABLE `feed`
  ADD PRIMARY KEY (`No`),
  ADD KEY `Drug` (`Drug`),
  ADD KEY `Chemical` (`Chemical`);

--
-- Indexes for table `fishpond`
--
ALTER TABLE `fishpond`
  ADD PRIMARY KEY (`No`),
  ADD KEY `ชนิดบ่อ` (`PondType`),
  ADD KEY `ชนิดสัตว์น้ำ` (`Species`);

--
-- Indexes for table `fishtype`
--
ALTER TABLE `fishtype`
  ADD PRIMARY KEY (`No`);

--
-- Indexes for table `pondtype`
--
ALTER TABLE `pondtype`
  ADD PRIMARY KEY (`No`);

--
-- Indexes for table `problem`
--
ALTER TABLE `problem`
  ADD PRIMARY KEY (`No`);

--
-- Indexes for table `report`
--
ALTER TABLE `report`
  ADD PRIMARY KEY (`No`),
  ADD KEY `บ่อที่ได้รับผลกระทบ` (`PondProblem`),
  ADD KEY `Event` (`Event`),
  ADD KEY `Problem` (`Problem`),
  ADD KEY `Treatment` (`Treatment`);

--
-- Indexes for table `status`
--
ALTER TABLE `status`
  ADD PRIMARY KEY (`No`);

--
-- Indexes for table `treatment`
--
ALTER TABLE `treatment`
  ADD PRIMARY KEY (`No`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `address`
--
ALTER TABLE `address`
  MODIFY `ID` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `address house reg`
--
ALTER TABLE `address house reg`
  MODIFY `ID` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `checklist`
--
ALTER TABLE `checklist`
  MODIFY `No` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `chemical`
--
ALTER TABLE `chemical`
  MODIFY `No` int(2) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `drug`
--
ALTER TABLE `drug`
  MODIFY `No` int(4) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `event`
--
ALTER TABLE `event`
  MODIFY `No` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `farm`
--
ALTER TABLE `farm`
  MODIFY `ID` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `feed`
--
ALTER TABLE `feed`
  MODIFY `No` int(4) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fishpond`
--
ALTER TABLE `fishpond`
  MODIFY `No` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `problem`
--
ALTER TABLE `problem`
  MODIFY `No` int(4) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `report`
--
ALTER TABLE `report`
  MODIFY `No` int(5) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `treatment`
--
ALTER TABLE `treatment`
  MODIFY `No` int(4) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `employee`
--
ALTER TABLE `employee`
  ADD CONSTRAINT `employee_ibfk_1` FOREIGN KEY (`address`) REFERENCES `address` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `employee_ibfk_2` FOREIGN KEY (`addressReg`) REFERENCES `address house reg` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `employee_ibfk_3` FOREIGN KEY (`FarmID`) REFERENCES `farm` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `farm`
--
ALTER TABLE `farm`
  ADD CONSTRAINT `farm_ibfk_1` FOREIGN KEY (`ChecklistNo`) REFERENCES `checklist` (`No`),
  ADD CONSTRAINT `farm_ibfk_2` FOREIGN KEY (`Pond`) REFERENCES `fishpond` (`No`),
  ADD CONSTRAINT `farm_ibfk_3` FOREIGN KEY (`Status`) REFERENCES `status` (`No`);

--
-- Constraints for table `feed`
--
ALTER TABLE `feed`
  ADD CONSTRAINT `feed_ibfk_1` FOREIGN KEY (`Drug`) REFERENCES `drug` (`No`),
  ADD CONSTRAINT `feed_ibfk_2` FOREIGN KEY (`Chemical`) REFERENCES `chemical` (`No`);

--
-- Constraints for table `fishpond`
--
ALTER TABLE `fishpond`
  ADD CONSTRAINT `fishpond_ibfk_1` FOREIGN KEY (`PondType`) REFERENCES `pondtype` (`No`),
  ADD CONSTRAINT `fishpond_ibfk_2` FOREIGN KEY (`Species`) REFERENCES `fishtype` (`No`);

--
-- Constraints for table `report`
--
ALTER TABLE `report`
  ADD CONSTRAINT `report_ibfk_1` FOREIGN KEY (`PondProblem`) REFERENCES `fishpond` (`No`),
  ADD CONSTRAINT `report_ibfk_2` FOREIGN KEY (`Event`) REFERENCES `event` (`No`),
  ADD CONSTRAINT `report_ibfk_3` FOREIGN KEY (`Problem`) REFERENCES `problem` (`No`),
  ADD CONSTRAINT `report_ibfk_4` FOREIGN KEY (`Treatment`) REFERENCES `treatment` (`No`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
