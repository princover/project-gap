import Vue from "vue";
import VueRouter from "vue-router";
import Login from "../views/login.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/signemp",
    name: "Signup-Emp",
    component: () => import("../components/signemp.vue"),
  },
  {
    path: "/signowner",
    name: "Signup-Owner",
    component: () => import("../components/signowner.vue"),
  },
  {
    path: "/Checklist",
    name: "Checklist",
    component: () => import("../components/ChecklistForm.vue"),
  },
  {
    path: "/test",
    name: "test",
    component: () => import("../views/test.vue"),
  },
  {
    path: "/test2",
    name: "test",
    component: () => import("../views/test2.vue"),
  },
  {
    path: "/",
    name: "Login",
    component: Login,
  },
  {
    path: "/about",
    name: "About",
    component: () => import("../components/About.vue"),
  },

  {
    path: "/signup",
    name: "signup",
    component: () => import("../views/Sign.vue"),
  },
  {
    path: "/Home",
    name: "Home",
    component: () => import("../views/Home.vue"),
    children: [
      {
        path: "dash",
        name: "dash",
        component: () => import("../components/Dashbord.vue"),
      },
      {
        path: "profile",
        name: "profile",
        component: () => import("../components/showprofileV2.vue"),
      },
      {
        path: "FeedForm",
        name: "FeedForm",
        component: () => import("../components/FeedForm.vue"),
      },
      {
        path: "problem",
        name: "problem",
        component: () => import("../components/ProblemForm.vue"),
      },
      {
        path: "FishPondForm",
        name: "FishPondForm",
        component: () => import("../components/FishPondForm.vue"),
      },
      {
        path: "givefoodform",
        name: "givefoodform",
        component: () => import("../components/givefoodForm.vue"),
      },
      {
        path: "ReportFeed",
        name: "ReportFeed",
        component: () => import("../components/ReportFeed.vue"),
      }, 
      {
        path: "ReportFood",
        name: "ReportFood",
        component: () => import("../components/ReportFood.vue"),
      }, 
      {
        path: "ReportFishPond",
        name: "ReportFishPond",
        component: () => import("../components/ReportFishPond.vue"),
      }, 
      {
        path: "ReportProblem",
        name: "ReportProblem",
        component: () => import("../components/ReportProblem.vue"),
      }, 
      {
        path: "Checklist",
        name: "Checklist",
        component: () => import("../components/ChecklistForm.vue"),
      },
      {
        path: "Report",
        name: "Report",
        component: () => import("../components/Report.vue"),
      }, 
      {
        path: "ReportChecklist",
        name: "ReportChecklist",
        component: () => import("../components/ReportChecklist.vue"),
      }, 

    ],
  },
  {
    path: "/Home2",
    name: "Home2",
    component: () => import("../views/Home2.vue"),
    children: [
    ],
  },
  {
    path: "/Home3",
    name: "Home3",
    component: () => import("../views/Home3.vue"),
    children: [
     
    ],
  },
  {
    path: "/Home4",
    name: "Home4",
    component: () => import("../views/Home4.vue"),
    children: [
      {
        path: "givefood",
        name: "givefood",
        component: () => import("../components/givefoodForm.vue"),
      },
      {
        path: "problem",
        name: "problem",
        component: () => import("../components/ProblemForm.vue"),
      },
     
    ],
  },
];
if (process.env.MODE !== "ssr") {
  routes.push({
    path: "*",
    component: () => import("../components/Error404.vue"),
  });
}

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
