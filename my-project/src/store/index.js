import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    userid:1111112252525,
    Farmid:2,
    EmpType:null,
    checklistid:null,
    pondid:null,
    lotid:null,
    feed:null,
    lat:'lat',
    lon:'lon',
  },
  mutations: {
    setuserid(state,userid){
      state.userid = userid
    },
    setFarmid(state,Farmid){
      state.Farmid = Farmid
    },
    setEmpType(state,EmpType){
      state.EmpType = EmpType
    },
    setchecklistid(state,checklistid){
      state.checklistid = checklistid
    },
    setpondid(state,pondid){
      state.pondid = pondid
    },
    setlotid(state,lotid){
      state.lotid = lotid
    },
    setfeed(state,feed){
      state.feed = feed
    },
    setlat(state,lat){
      state.lat = lat
    },
    setlon(state,lon){
      state.lon = lon
    },
  },
  actions: {
  },
  modules: {
  }
})
